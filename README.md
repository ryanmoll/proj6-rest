# Project 6: Brevet time calculator service


Author: Ryan Moll


Overview:

- This project is a calculator built to calculate open times and close times for checkpoints in a brevet. It does so using Flask to handle the page serving and mongoDB to store the data. It also offers APIs which can be accessed to get the open and close times in JSON or CSV format.

- To run the project, execute the commands "docker-compose build" and "docker-compose up" to build the docker containers and run them. Next, you should navigate to the page at "http://0.0.0.0:5001/" in a browser. This is the html landing page where you can plan out our brevet and submit your data. The page will do its best to prevent you from submitting invalid data or displaying if the database is empty. This is revealed if you hover your mouse over the disabled buttons. Once you have entered valid data and submitted, you can view your data with the 'display' button.

APIs:

- Now that you have submitted your data, it is stored in a mongoDB database. We can access this data via a few simple APIs:

    > http://0.0.0.0:5001/listAll or http://0.0.0.0:5001/listAll/json will provide both the open and close times for each checkpoint in JSON format

    > http://0.0.0.0:5001/listOpenOnly or http://0.0.0.0:5001/listOpenOnly/json will provide only the open time for each checkpoint in JSON format

    > http://0.0.0.0:5001/listCloseOnly or http://0.0.0.0:5001/listCloseOnly/json will provide only the close time for each checkpoint in JSON format

    > http://0.0.0.0:5001/listAll/csv will provide both the open and close times for each checkpoint in CSV format

    > http://0.0.0.0:5001/listOpenOnly/csv will provide only the open time for each checkpoint in CSV format

    > http://0.0.0.0:5001/listCloseOnly/csv will provide only the close time for each checkpoint in CSV format

    Note: The CSV endpoints will automatially download the CSV formatted data as a .csv file onto your computer. From there you can open the data in excel or use it however you like.

- Each endpoint can be limited with the 'top' parameter. Simply add "?top=<int>" to make the api return only the top 'k' values of the query where 'k' is the integer you passed in as the top parameter.

- If you want a more readable version of the data that is provided by these APIs, you may navigate to http://0.0.0.0:5000/ where you'll find a landing page with 3 options. The options will redirect you to either http://0.0.0.0:5000/listAll.php, http://0.0.0.0:5000/listOpenOnly.php, or http://0.0.0.0:5000/listCloseOnly.php. At these different pages, you can find the same data provided by the APIs only parsed into a more readable format.

	- Each of the "client facing" php pages can also be appended with a URL parameter of the same form "?top=<int>" to restrict the number of results that are shown on that page. For example the URL http://0.0.0.0:5000/listCloseOnly.php?top=4 would show the close times of the top 4 checkpoints only. If no paramete is provided it will simply display all of the checkpoints. 